# Request what you need.

At Codeberg, we try hard to provide everyone with what they need to develop their Libre Software without friction.
However, we restrict certain things to limit abuse of our infrastructure.

If you hit some of the limits we set,
please kindly ask us here.

> **Warning**
> This is not a good place to request features.
> Things that require implementation work by our team
> are a better fit for [the community issues section](https://codeberg.org/Codeberg/Community/issues/).


## Woodpecker CI

If you want to try our hosted Woodpecker CI,
you need to request access per user account.
It is currently not possible to unlock access per repository or organization,
so if multiple team members need to configure settings,
please list them all.

<details><summary>Read on ...</summary>

### Disclaimers and Caveats

Although we and the awesome maintainers behind Woodpecker CI do a lot to create an awesome user experience, the rocket start in development activity only occured very recently. Since we are always running latest edge code, you might notice bugs, especially in the UI/UX, or lack features of proprietary competitors.

Also, we at Codeberg do our best to keep the hosted service stable and accessible, but we do not make any guarantees of any kind.

CI access is provided as-is and might break at any time and for an undefined period of time, due to server issues, for testing and maintenance purpose or human error.

### Request procedure

Please tell us for which projects you'd like to test CI access for. Also note, that we might reject requests from new Codeberg users, as we want to prevent abuse of our computing power via spam accounts. If you just joined Codeberg for testing CI access, please give us a link where we can find your previous projects / contributions.

For the usage of the Codeberg Hosted CI, the following rules apply:

- All code must be carrying a valid Free/Libre Open Source Software license as defined by the FSF/OSI (note this is a general requirement for code hosting at Codeberg).
- The repository needs to be publicly accessible. In case you need pre-release / staging areas, please explicitly request this in the issue tracker.
- The project's purpose must be described briefly in the README or repository description, and match the implementation.
- Resource usage must be reasonable for the intended use-case. This is determined on a case-by-case basis, but please be aware that CI uses a lot of computing resources (cloning your repo and container, installing all your required tools, building and throwing every thing away) which costs us money and does damage to our precious environment. Therefore, please consider twice how to create a good balance between ensuring code quality for your project and resource usage therefore.
    - If you are using the CI to build with Rust, please limit the max number CPUs it uses. You can do so with the `-j` parameter (e.g. `-j4` to use 4 CPUs). This is necessary, as Rust by default uses all CPUs, taking up all resources from other users.

</details>

## Repo limit

There is currently a default limit of 100 repositories per user / org.
It can easily be lifted per account / organization.


## Private repositories

Storing private repositories is allowed for things related to Free Software,
or small content like personal notes.
It is **limited to 100 MiB** per user.

If you need more for Free Software projects,
for example to prepare a release
please ask us.

Please note that we cannot grant exceptions for proprietary software here,
because this is not in scope of our non-profit organization.


## Weblate users for remote forges

If you want to use Codeberg Translate but have the instance create pull requests on remote servers,
an admin needs to configure it accordingly.

Please ask us.

## Stickers

Requesting stickers should be preserved for redistribution: Hackerspaces, schools and associations, work / offices and events. We are happy to help distribute stickers for these purposes. Once approved, a Codeberg member can coordinate shipping to a specified address.

