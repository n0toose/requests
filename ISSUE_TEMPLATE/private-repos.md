---
name: "Allow private repositories"
about: "If you require extended private storage for your FLOSS projects, ask us here."
title: "[PRIV] "
labels: resources/private
---

Hi, my FLOSS project requires more than 100 MiB of private storage.

For developing the FLOSS project(s) _______, I require

- [x] one
- OR
- [ ] multiple

projects with the total size of _______ MiB.

- [x] I request this exception for the following period: ______ (maximum two years, renew regularly if you need longer)
- OR
- [ ] I would like this exception to be permanent (for small use cases like security preparation / team discussion)

Please tell us about the motivation for private storage:

- [ ] I plan to publish the content as Free Software, but would like to prepare in private
- [ ] We require internal team discussions
- [ ] We need to have a permanent area to prepare private (e.g. security) patches for the following public projects: 


- [x] I will renew my request if my private repository storage exceeds the requested limit


Applies to timespans only: What shall we do when the period passes AND there was no request for extending the period AND we do not receive a reply to our notification within 30 days AND the content is available under a libre License?

- [ ] Delete my content
- OR
- [ ] Publish my private work to the public
- [ ] My private work is Free Software, too, and everyone with access to it can publish it as covered by the License

Further comments:
