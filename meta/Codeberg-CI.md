# Codeberg CI: Dealing with access requests

This is a living document meant to provide a set of guidelines for
the people that review the requests for [Codeberg CI][codeberg-ci].

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and
"OPTIONAL" in this document are to be interpreted as described in
[RFC 2119][rfc].

This document shall be authoritative and shall serve as a reference,
but it cannot be exhaustive: People are not as predictable as computers.

If you have any questions, it is recommended that you reach out to
another reviewer or admin.

Usage instructions for the [cibot][cibot] SHALL be found in the
[cibot repository][cibot].

[codeberg-ci]: https://ci.codeberg.org
[rfc]: https://www.rfc-editor.org/rfc/rfc2119
[cibot]: https://codeberg.org/Codeberg-CI/cibot

---

# Criteria

Providing people access to the Codeberg CI is, most of the time,
a straightforward process comprising of three criteria:

1. **Licensing:** The linked projects SHOULD contain an FSF or OSI-approved **license**.
2. **Content:** The repositories MUST adhere to the Terms of Use.
3. **Content:** The profiles of the users requesting access MUST adhere to the Terms of Use.

**FSF and OSI-approved licenses** are **always** considered to be *acceptable*.

---

However, those official rules may not always be the best option,
as there are some cases that are not as clear.

Not all projects hosted on Codeberg are code,
and some projects even use a combination of licenses.
The rest of this document will try to explain some real-world
edge cases, and how a **reviewer** is expected to treat them.

This document uses the following terms:
- **Requesters**: Persons initiating a request under
  [Codeberg-e.V./requests][requests]
- **Reviewers:** Persons reviewing requests by **requesters**.
- **Admins:** Persons that control Codeberg's infrastructure
  or the list of reviewers.
- **Assets:** Textures, music, fonts and other creative works that
  do not fall under the category of source code.

[requests]: https://codeberg.org/Codeberg-e.V./requests

## Content

### The repository that the user has linked is empty.

- **Is the repository currently present outside of Codeberg?**
  - Some people request CI access in advance **before** migrating to Codeberg.
  - This **SHOULD** be mentioned by the requester.
  - **Solution:** Check if the project's repository is currently present on another platform (e.g. GitHub) and if it is appropriately licensed.
    - If you are **not** provided a link to the project:
      - You **MAY** ask the requester for more information.
      - You **MAY** look up the project on other platforms yourself,
        if it is not too much trouble. It is the responsibility of
        the requester to provide precise information.

- **Does the link to the repository on Codeberg return a 404 error?**
  - The project is possibly private. We **MAY NOT** provide access to private repositories.
  - **Solution:** Ask the user to make their repository public.
  - **Exception:** The requester has adequately answered the following questions:
    - "Why is the repository private for the time being?"
    - "What is the project intended for?"
    - "Will it be published at a later point in time?"
    - "Will it be published under the terms of an **acceptable license**?"

    If the requester has not answered those questions, the reviewer may
    decide whether they wish to ask these questions or simply mention
    that the repository is private, and cannot be reviewed.

The definition of **acceptable** is defined in the **Licensing** section.

### The requester wants to use the CI to build with Rust  

As a reviewer, if a requester wants to use the CI to build tools in Rust, check potentially existing CI-workflows for the amount of CPUs it uses (i.e. Do the workflows set the `-j` parameter?). If the number of CPUs is not limited, you should remind the requester to limit the maximum number of CPUs in their build stages, as Rust will otherwise take up all resources. 

Similarly, when a requester has no CI workflows defined yet, the reviewer should remind them to set an appropriate `-j` flag (e.g. `-j4` to request 4 CPUs) for their workflows.

## Licensing

### A repository is not licensed using an FSF or OSI-approved license.

Not **all** projects hosted on Codeberg comprise of source code,
and some projects even use a combination of licenses.

For this reason, we MAY choose to be more lenient with the requirement
for **FSF or OSI-approved** license, especially when the licensed material
does **not** concern software.

#### Acceptable Licenses

On top of FSF and OSI-approved licenses, we explicitly consider
the following licenses to be acceptable under **specific, loose exceptions**:
- [CC BY-NC-SA][cc-by-nc-sa]
- [CC BY-SA][cc-by-sa]
- [CC BY][cc-by]

We **MAY** leave room for websites that find themselves in a gray line, if
the content hosted on them aligns with the goals of our association as described
in our [Bylaws](https://codeberg.org/Codeberg/org/src/branch/main/en/bylaws.md).

We MAY leave room for other licenses (including, but not limited to licenses by
Creative Commons), as long as they represent a
["copyleft" spirit][copyleft].

Such exceptions are evaluated on a strict case-by-case basis.
Please ask another **reviewer** or **admin** before taking action.

[cc-by-nc-sa]: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/deed.en
[cc-by]: https://creativecommons.org/licenses/by/4.0/deed.en
[copyleft]: https://en.wikipedia.org/wiki/Copyleft

#### Exceptions

This is not a definitive list of exceptions,
and each case has to be examined individually.

- The project is a **website**.
- The project is a **video game** that is using assets...
  - from third-parties
  - people that cannot be reached anymore.
- The project is a **multimedia archive**, such as a podcast from 10 years ago.
  - In the case of **archives**, other platforms, such as the
    [Internet Archive][ia], could work better for the requester.
    Whether such platforms are mentioned is to be decided by the reviewer.

[ia]: https://archive.org

### Websites

Many users use Codeberg, and, in turn, Codeberg Pages, as a "drop-in" replacement
for [GitHub Pages][gh-pages] and [Neocities][neocities].

Those users often rely on the CI for automatically deploying new content
(i.e. blog posts) after issuing a commit to a repository.

[gh-pages]: https://pages.github.com
[neocities]: https://neocities.org

##### The website's repository contains no license.

If **any** repository does **not** contain a license, the requester **SHOULD** explain why.

If you still want to take a look regardless, we advise taking the following steps:

- **Check a live version of the website,** if possible.
- Take a look at whether **some** content, such as a blog post,
  is distributed under an **acceptable** license.

The decision is yours. Nevertheless, we recommend reaching out to another **reviewer**.
