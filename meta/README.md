# `meta` - Codeberg e.V./requests

This repository contains guidelines for reviewers processing requests
on [Codeberg-e.V./requests].

[Codeberg-e.V./requests]: https://codeberg.org/Codeberg-e.V./requests
